FROM debian:buster-slim

ARG GITHUB_RUNNER_VERSION="2.278.0"

ENV RUNNER_NAME "runner"
ENV RUNNER_WORKDIR "_work"

RUN apt-get update && apt-get -y install curl git sudo jq cpu-checker && apt-get clean
RUN rm -rf /var/lib/apt/lists/*
RUN useradd -m github
RUN usermod -aG sudo github
RUN echo "%sudo ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers

USER github
WORKDIR /home/github

RUN curl -Ls https://github.com/actions/runner/releases/download/v${GITHUB_RUNNER_VERSION}/actions-runner-linux-x64-${GITHUB_RUNNER_VERSION}.tar.gz | tar xz \
    && sudo ./bin/installdependencies.sh

COPY --chown=github:github entrypoint.sh ./entrypoint.sh
RUN sudo chmod u+x ./entrypoint.sh

ENTRYPOINT ["/home/github/entrypoint.sh"]
