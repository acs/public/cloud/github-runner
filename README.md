# HermitRunner

This repository creates and deploys a self-hosted GitHub Actions runners in our Kubernetes cluster.
The design based on the blog post [Running self-hosted GitHub Actions runners in your Kubernetes cluster](https://sanderknape.com/2020/03/self-hosted-github-actions-runner-kubernetes/).
